from django.apps import AppConfig


class ProfileDatabaseConfig(AppConfig):
    name = 'profile_database'
