# Generated by Django 3.1.5 on 2021-01-28 03:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('profile_database', '0003_auto_20210128_0228'),
    ]

    operations = [
        migrations.AlterField(
            model_name='image',
            name='image',
            field=models.ImageField(upload_to=''),
        ),
    ]
