from django.db import models

# Create your models here.
class Message (models.Model):
    name = models.CharField(max_length=20)
    phone = models.CharField(max_length=20)
    email = models.CharField(max_length=120)
    content = models.TextField()

    def __str__(self):
        return self.name

class Image (models.Model):
    image = models.ImageField(upload_to='saveImages/')