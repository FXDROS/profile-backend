from .views import MessageListView, ImageListView
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'message', MessageListView, basename='message')
router.register(r'image', ImageListView, basename='image')
urlpatterns = router.urls