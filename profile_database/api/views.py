from profile_database.models import Message, Image
from .serializers import MessageSerializer, ImageSerializer
from rest_framework import viewsets

class MessageListView(viewsets.ModelViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer

class ImageListView(viewsets.ModelViewSet):
    queryset = Image.objects.all()
    serializer_class = ImageSerializer