from django.contrib import admin
from .models import Message, Image

# Register your models here.
admin.site.register(Image)
admin.site.register(Message)
